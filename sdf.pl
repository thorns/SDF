#! /usr/bin/perl -w
#/*@@
#  @file      sdf.pl
#  @date      Sat 12 June 2004
#  @author    Thomas Radke
#  @desc
#             Finds an external SDF installation
#  @enddesc
#  @version   $Header$
#@@*/

# SDF_DIR must be given be the user
my $bbhutil_dir = $ENV{'SDF_DIR'};
if (! $bbhutil_dir)
{
  print "BEGIN ERROR\n";
  print "SDF requires an external SDF installation. Please specify " .
        "SDF_DIR to point to this installation or remove thorn SDF " .
        "from your ThornList !\n";
  print "END ERROR\n";
  exit (-1);
}

print <<EOF;
BEGIN MAKE_DEFINITION
HAVE_SDF = 1
END MAKE_DEFINITION
INCLUDE_DIRECTORY $bbhutil_dir/include
LIBRARY           bbhutil sv
LIBRARY_DIRECTORY $bbhutil_dir/lib
EOF

exit (0);
